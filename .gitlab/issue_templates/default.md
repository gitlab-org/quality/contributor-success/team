<!--
Please verify the issue you're about to submit isn't a duplicate.
Explain the reason for submitting this issue, such as a bug report, feature request, or question.
For feature requests, please include the WHY as well as the WHAT.
-->

## Recreation steps

<!-- For bugs please complete this section. -->

## Implementation plan

<!-- If you can provide any steps around how to address this issue, please do so. -->

/label ~"Contributor Success" ~"workflow::validation backlog"  
/cc @gitlab-org/developer-relations/contributor-success 
