# Co-Create Project Checklist

## Project Information
- Customer:  
- Account Team: 
    - CSM/CSA: @
    - AE: @
    - SA: @
- Co-Create Project Manager (CCPM): @  
- Assigned Engineer: @

## Planning Phase

### CSM/CSA Tasks
1. [ ] Merge Requests Selection
   1. [ ] Primary MR identified and approved by product
   1. [ ] 2-3 backup MRs identified and approved by product
   1. [ ] MRs complexity assessed and confirmed as achievable within timeframe
   1. [ ] Created tracking issue in gitlab-org project with co-create label

1. [ ] Legal Requirements
   1. [ ] Corporate Contributor License Agreement signed
   1. [ ] All contributing developers identified

### CCPM Tasks
1. [ ] Engineer Assignment
   1. [ ] Engineer with relevant expertise identified
   1. [ ] Engineer availability confirmed
   1. [ ] Engineer added to project Slack channel

## Preparation Phase

### CSM/CSA Tasks
1. [ ] Schedule Onboarding Workshop
   1. [ ] Required attendees confirmed
   1. [ ] Workshop completed

### Engineer Tasks
1. [ ] Travel Logistics
   1. [ ] Visa requirements checked
      1. [ ] If needed: Visa application submitted and approved
   1. [ ] Navan access confirmed
   1. [ ] Uber for Business access setup
   1. [ ] Transportation booked
   1. [ ] Hotel accommodations booked
   1. [ ] Travel itinerary shared with team

1. [ ] Technical Preparation
   1. [ ] Customer background review completed with CSM/CSA
   1. [ ] Technical documentation reviewed
   1. [ ] Development environment tested
   1. [ ] Initial code review completed

### CSM Tasks
1. [ ] On-site Week Planning
   1. [ ] Dates confirmed with all participants
   1. [ ] Customer team 75% availability confirmed
   1. [ ] Meeting room/workspace arranged
   1. [ ] Access badges/security clearance arranged

### Engineer Tasks
1. [ ] Daily Schedule Planning  
Example schedule:
* Day 1: Kick-off, confirm dev environment and access
* Day 2-4: Development of feature X
* Day 5: Wrap-up and next steps  
Consider adding daily stand-ups with CSM and CCPM if useful

## Retrospective Phase

### CCPM Tasks
1. [ ] Create internal retrospective issue
1. [ ] Create external retrospective issue
1. [ ] Share external issue with customer on last day of on-site week

/label ~co-create ~customer-collaboration
/confidential 