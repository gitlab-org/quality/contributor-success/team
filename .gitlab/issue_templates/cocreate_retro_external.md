# Async External Cocreate Retrospective

This is an asynchronous retrospective for the OnSite visit ENGINEER at CUSTOMER for the CoCreate onsite workshop.

This issue is private (confidential), but is shared with the customer.

Involved people:

- EXT-PARTICIPANT-BULLETS

Please look at back at your experiences working on this initiative and especially the on-site visit, ask yourself:

**:star2: what praise do you have for the group?**,
**:+1: what went well this visit?**, **:-1: what didn’t go well this
visit?**, and **:chart_with_upwards_trend: what can we improve going
forward?**, and honestly describe your thoughts and feelings below.

For each point you want to raise, please create a **new discussion** with the
relevant emoji, so that others can weigh in with their perspectives, and so that
we can easily discuss any follow-up action items in-line.

'Emotions are not only allowed in
retrospectives, they should be encouraged', so we'd love to hear from you here
if possible.

Please do feel free to include any additional participants we failed to mention above!

/confidential
/assign EXT-PARTICIPANT-LIST
