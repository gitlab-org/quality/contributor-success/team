## Leading Organization Individual Issue

This confidential issue follows an individual Leading Organization for next steps, check-ins and requests. Assign the DRI for the Leading Organization and due date for the next check in.

### Leading Organization Summary 

Include a quick glance summary of this Leading Organization.

### Contribution Interests

Summarize any contribution interests or goals for this Leading Organization.

### Contacts

- DRI: Contributor Success Team Member acting as DRI
- Account Manager: Team Member acting as Account Manager
- Leading Organization Contact/Email: 

### Notes

- Salesforce Link: link to Salesforce notes
- Onboarding Doc: link to onboarding documentation
- Technical Workshop Doc: link to technical workshop documentation

## Status

Summarize the status of the Leading Organization, any problem areas or successes. Include thoughts on how the organization is feeling about contributing and their comfort level in moving forward.

### Next steps

The very next action steps to move this Leading Organization forward:

- [ ] DRI to follow up on request for training documentation
- [ ] Help get first MR submitted
- [ ] Invite organization to their Slack channel

### Tasks/Itches to Scratch

Ongoing list of items to address with this Leading Organization:

- [ ] DRI to inquire about funding GitLab training programs for Leading Organization
- [ ] Demo GitPod/GDK development environments
- [ ] Get eyes on old issue and add labels
- [ ] Help establish an open source contribution policy for the organization
- [ ] Help start MR to attach design prototypes
- [ ] Address gaps in GraphQL interface

### Contributions

#### Issues

Include links to open issues from this Leading Organization:

- link 1
- link 2

#### Merge requests

Include link to this Leading Organization's MR chart from the individual dashboard found here: https://app.periscopedata.com/app/gitlab/1061457/Leading-Org-individual-information

- chart link

/confidential
