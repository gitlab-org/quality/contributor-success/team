## Contributor EE license request

This issue is for a Contributor to request a development EE license.

As per the directions in the [handbook](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows#contributing-to-the-gitlab-enterprise-edition-ee),
an initial 90 day license will be issued.
This can be extended upon expiry by making a new request in this project. 

A new license will be:

* 90 days
* 100 seats

### Required information from contributor 

Please fill out the following information:

- [ ] Name:
- [ ] Email:
- [ ] Company:
- [ ] gitlab.com username:
- New request or renewal?
  - [ ] New Request
  - [ ] Renewal
- If Renewal:
  - [ ] Previous license seats:
  - [ ] Previous expiration date:

Pinging @gitlab-org/developer-relations/contributor-success for visibility.

### Process for Contributor Success team member

* Complete the [internal support request form](https://support-super-form-gitlab-com-support-support-op-651f22e90ce6d7.gitlab.io/)

/confidential
