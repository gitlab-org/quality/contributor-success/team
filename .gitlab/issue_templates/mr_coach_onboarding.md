## [COACH-NAME] Merge Request Coach Onboarding

Thank you for becoming a Merge Request coach at GitLab!
Merge Request coaching is fulfilling to many people because it allows you to interact and support developers from the broader open-source ecosystem while they implement GitLab features.

This issue is a checklist that helps you become a professional Merge Request coach.

If you're new to GitLab, we recommend you follow at least some of the steps tagged (optional) below.

### Manager section

- [ ] fill in the name of the aspiring MR coach into the `COACH-NAME` placeholder in the issue title

### Apply to become a MR coach

- [ ] Create an MR to add "Merge Request Coach" to "departments" and "expertise" sections in your team page entry.
  - [example](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/102653)
- [ ] Add at least [one project and role](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person#data-structure) to your team entry page.
- [ ] Use the [Merge Request Coach MR template](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Merge%20Request%20Coach.md) to explain your motivation in the MR body:
   - Why you want to become a Merge Request Coach?
   - How much time you are planning to spend on it?
   - Which duties you are focusing on (e.g. triage, finish stale MRs)?
- [ ] Mention `@gitlab-org/developer-relations/contributor-success` in the MR so they are aware of the application.
- [ ] Mention your manager and ask them to merge it.
- [ ] Once accepted, one of the coaches with Owner permissions will add you to the `@gitlab-org/coaches` group.

### Learn to become an MR coach section

- [ ] read the [Merge Request Coach handbook page](https://handbook.gitlab.com/job-families/expert/merge-request-coach/)
- [ ] read the [contributing guidelines](https://docs.gitlab.com/ee/development/contributing)
- [ ] read all parts of the [development process](https://docs.gitlab.com/ee/development/development_processes.html)
- [ ] learn about reviewer roulette: [review](https://about.gitlab.com/blog/2019/10/23/reviewer-roulette-one-year-on), [handbook page](https://docs.gitlab.com/ee/development/code_review.html#reviewer-roulette)
- [ ] read the [Contributor Success workflows](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/community-contributors-workflows)
- [ ] join the `#mr-coaching` Slack channel
- [ ] from a private GitLab account, use the [contributing guidelines](https://docs.gitlab.com/ee/development/contributing) to create and submit at least one contribution to the codebase you are coaching in order to experience the process that contributors from the wider community typically go through
- [ ] add missing information to the [contributing guidelines](https://docs.gitlab.com/ee/development/contributing)
- [ ] (optional) shadow a Contributor Success team member while they coach a merge request
- [ ] (optional) coach at least one merge request under the guidance of a Contributor Success team member
- [ ] (optional) shadow a Merge Request Coach outside the Contributor Success team while they coach a merge request
- [ ] (optional) coach at least one merge request under the guidance of a MR coach outside the Contributor Success team
