## Customer contributor onboarding

This issue captures the tasks for onboarding customer contributors 

### Goal 

Give the customer an overview of our contributor process, communicate to the customer on the benefits of contributing & familarize the customer with the teams at GitLab.

Slide: https://docs.google.com/presentation/d/1gmA0YevY7NcnMlDWL462QSau5HFN4mKvejrGyF0_RIc/edit#slide=id.g128d085a7e0_0_471

### Tasks

- [ ] TAM to create this issue and fill in the customer information as well as TAM contact point. 
  - Customer: `Brief customer information, tier`
  - TAM: `Add TAM handle`
- [ ] Setup 30 mins onboardig with Customer, TAM to help facilitate
- [ ] Give Customer overview of slide deck and answer questions
- [ ] Identify areas of interest to coordinate with product management and engineering teams 
- [ ] Identify customer contributors to add to MRARR tracking 
- [ ] Improve onboarding slide from feedback 

/confidential
