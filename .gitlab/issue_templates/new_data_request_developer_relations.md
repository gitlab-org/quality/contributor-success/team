## Problem (Please explain what data is needed and why)
<!-- e.g. We launched the Co-create campaign, but we need a dashboard to track activities from targeted accounts.
Part of the Q2 OKR is to assess the impact of the Co-create campaign and determine if there is enough increased
activity to continue it in Q3. -->

## Business use case (Please explain what this data will be used for):
<!-- e.g. It will help our Contributor Success team broadcast the the effectiveness of the Co-create campaign across Developer Relations, Marketing, Sales and Engineering divisions. -->

- Is there a valid business use case?
- Who will benefit from this data? Team / Department / Division / Multiple Divisions / Enterprise
- What revenue or efficiency impact will this data have?

## Severity

* Indicate the severity level of this request:
  - [ ] Critical
  - [ ] High
  - [ ] Medium
  - [ ] Low
* [ ] Reasoning for selected severity level
<!-- e.g. This dashboard will measure the success of the co-create campaign and guide decisions on whether to continue it in the next quarter. -->
  - `     `
* [ ] Indicate time frame this data request is needed by
<!-- e.g. 3 months from today -->
  - `     `

## Request checklist

To support the new data request triage process, please complete the checklist below. If anything is uncertain or unknown, you may skip that point.
* [ ] Prefix the issue name with 'New DevRel Data Request:', e.g. 'New DevRel Data Request: Co-create Data Dashboard'.
* [ ] Review the [current data sources available in the EDW](https://handbook.gitlab.com/handbook/enterprise-data/platform/#data-sources) and confirm it concerns a new data request, or a change/extension of an existing data or dashboard:
  - [ ] Completely new data request
  - [ ] Change/extension of an existing data or dashboard
* [ ] Does it contain MNPI data (see the [SAFE guide](https://handbook.gitlab.com/handbook/legal/safe-framework/#sensitive) for more information about MNPI)?
* [ ] Does it contain [Personal Data](https://handbook.gitlab.com/handbook/security/data-classification-standard/#data-classification-definitions)?  
* [ ] How often does the data need to be refreshed?
<!-- e.g. Data needs to be refresh once per day -->
  - `     `

## People matrix

| Role | Name | Gitlab Handle |
| ---- | ---- | ------------- |
| Technical contact for data related questions | `Please provide` | `Please provide` |
| Data access approval* | `Please provide` | `Please provide` |
| Business users who need to be informed in case of data outage | `Please provide` | `Please provide` |

* Data access approval will be involved in the Access Request process and need to give approval if a GitLab team member applies for raw data access.

<!-- Do not edit below this line -->

/label ~"Contributor Success" ~"workflow::validation backlog"
/assign @mxiao-ext
