## Create a How-To Video "[VIDEO-NAME]"

This is a checklist of recommended items to be applied to a how-to video.

### Apply these to your video

- [ ] Video is dedicated to one topic
- [ ] Video length is not too long (20 - 30 mins is a max)
- [ ] Introduction
    - Introduction is simple and to the point with valuable and important information within the first 30 seconds to 1-3 minute
- [ ] The main part
    - [ ] Contains step-by-step instruction(for example, Reproducing the issue, Analyzing the code, Finding the root cause, Solution, Testing and verification)
    - [ ] If sharing the code, ~30 lines of the code are visible on a screen
- [ ] Conclusion
    - [ ] Summarize what was discussed in the video
    - [ ] Have a call to action

### Posting on YouTube

- [ ] The title of the video describes exactly what the video is about
- [ ] Segmentation of the video with timestamps is added
- [ ] Description of the video:
    - [ ] A keyword descriptive sentence as the first line
    - [ ] Clearly lays out the topic and quickly explains what the viewers can expect to learn
    - [ ] Contains links to resources that are mentioned in the video (issue, merge request, documentation, video, etc.) and:
        - [ ] https://about.gitlab.com/community/contribute/
        - [ ] Discord channel
    - [ ] Contains a list of video fragments with timestamps and a short description of each

/assign me
/confidential
