## Summary

Workflow issue to track steps to select and reward GitLab MVPs.

### Resources

* [Full instructions for GitLab MVP selection process in the handbook](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/mvp-process/)
* [Active MVP nominations issue](https://gitlab.com/search?search=%22GitLab+MVP+Nominations%22&project_id=39971471&group_id=65123486&scope=issues&state=opened)

### Steps

Internal steps to track completion of the MVP selection:

- Complete these steps at least 10 calendar days before the release date
  - [ ] 1. MVP(s) selected from rolling nominations issue
  - [ ] 2. MVP(s) announced in new thread of nominations issue
  - [ ] 3. Merge request drafted to add MVP(s)
- Complete these steps by Tuesday of release week
  - [ ] 4. Merge request reviewed and merged
  - [ ] 5. MVP achievement badge awarded
  - [ ] 6. Swag prize/tree option issued
- Complete these steps after release post is live
  - [ ] 7. Announcement message of MVP(s) with **live** release post link and **nominations issue reminder**
    shared in #whats-happening-at-gitlab Slack channel
  - [ ] 8. Slack message forwarded to #developer-relations, #mr-coaching, and #core
  - [ ] 9. Announcement message shared in Discord #announcements channel

/assign me
/label ~"Contributor Success" 
/label ~"contributorgrowth::increase value"
/label ~"workflow::in dev"
