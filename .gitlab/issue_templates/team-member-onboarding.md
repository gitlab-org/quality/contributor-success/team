## Welcome

Welcome to the Contributor Success team! This issue serves as your Contributor Success specific
onboarding hub. It contains various tasks that gets you familiar with the team
mission, strategy, objectives, metrics and processes.

- [ ] Name this issue `[YOUR_NAME] Onboarding`
- [ ] Assign it to the new team member
- [ ] Create an MR coach onboarding issue and fill in its URL into the `MR-COACH-ISSUE-URL` placeholder below

## General team information

Important handbook pages & information on how we work and collaborate with our conterparts.

- [ ] Read the [Contributor Success handbook page](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success)
- [ ] Read the [Leading Organization's strategy](https://handbook.gitlab.com/handbook/marketing/developer-relations/leading-organizations/)
- [ ] Read the [Open Source Growth strategy](https://handbook.gitlab.com/handbook/engineering/open-source/growth-strategy.html)
- [ ] Read our [company strategy and goals](https://about.gitlab.com/company/strategy/)
- [ ] Read our [contributor increase strategy](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/#strategy)
- Familiarize yourself with our team KPIs
  - [ ] [Unique Community Contributors per Month](https://handbook.gitlab.com/handbook/marketing/developer-relations/performance-indicators/#unique-wider-community-contributors-per-month)
  - [ ] [Community Coaches per Month](https://handbook.gitlab.com/handbook/marketing/developer-relations/performance-indicators/#community-mr-coaches-per-month)
  - [ ] [Community Contribution MRs as Features per Month](https://handbook.gitlab.com/handbook/marketing/developer-relations/performance-indicators/#feature-community-contribution-mrs)
    - [ ] [MRARR](https://handbook.gitlab.com/handbook/marketing/developer-relations/performance-indicators/#mrarr)
  - [ ] Explore our [Wider Community PIs dashboard](https://app.periscopedata.com/app/gitlab/729542/Wider-Community-PIs)
  - [ ] Explore our [Leading Organizations PIs dashboard](https://app.periscopedata.com/app/gitlab/1064389/Leading-Organizations-PIs)
- Review last Quality Key review
  - [ ] [Slides Q2](https://docs.google.com/presentation/d/12PPkin4s2T7U9_TPsofvNPyzGsN9EF8GOLW3WpNoFBs/edit)
  - [ ] [Slides Q3](https://docs.google.com/presentation/d/1YYAM1LbJvXRb2A-ETJVK7XnpNY9IbNFnbt47zRI4wK4/edit)
  - [ ] [Notes](https://docs.google.com/document/d/1xbXfSwHrJZvtJIpj0t45IkWy_sEzEXbDj_56dSSJTQc/edit#)
  - [ ] Recording [March Key Review](https://www.youtube.com/watch?v=MAZzfVaQcwg&ab_channel=GitLabUnfiltered)
- [ ] Explore the [Contributor Success Team issues](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/?sort=created_date&state=opened&first_page_size=100)
- [ ] Explore our Contributor Growth dashboard and see what stands out that peaks your interest. Let the team know. https://gitlab.com/groups/gitlab-org/-/boards/4879276
  - [ ] [Community Contributions dashboard](https://app.periscopedata.com/app/gitlab/729542/Wider-Community-PIs)
  - [ ] [Leading Org dashboard](https://app.periscopedata.com/app/gitlab/1064389/Leading-Organizations-PIs)
- [ ] Review [FY24Q1 OKR progress](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/145)
- [ ] Review [FY24Q2 OKR progress](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/2011)
- [ ] [Agenda](https://docs.google.com/document/d/11S48qIoFzKxZAdMznlUm5-Pob4EEBb8DNv2YIBLw9uw/edit#heading=h.dqzrga7kvfm5) of the Contributor Success & Growth stand-up

## Our counterparts & contribution process

- [ ] Read the [Merge Request triage process](https://handbook.gitlab.com/handbook/engineering/quality/merge-request-triage/)
- [ ] Read the [Developer Relations team page](https://handbook.gitlab.com/handbook/marketing/developer-relations/)
- [ ] Read our [Core team information](https://handbook.gitlab.com/handbook/marketing/developer-relations/core-team/)
  - Our [Core team page](https://about.gitlab.com/community/core-team/)
- [ ] Read our [MR Coach & their responsibilities](https://handbook.gitlab.com/job-families/expert/merge-request-coach/#responsibilities)
  - A list of current MR coaches [is in this page](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/merge-request-coach-lifecycle.html#current-merge-request-coaches)
- [ ] Review our [community portal](https://about.gitlab.com/community/)
- [ ] Review our [developer portal](https://developer.gitlab.com/)
- [ ] Read the [code review documentation](https://docs.gitlab.com/ee/development/code_review.html)
- [ ] Review our [contributing to GitLab docs](https://docs.gitlab.com/ee/development/contributing/)

## Slack channels

Join these Slack channels:

- [ ] #community-relations
- [ ] #core
- [ ] #g-contributor-success
- [ ] #i_user_engagement_contributor_growth
- [ ] #leading-organizations
- [ ] #mr-coaching
- [ ] #quality
- [ ] #gitter-contributors-room
- [ ] #ux-community-contributions

Join our discord contributors channel:
- [ ] https://discord.gg/gitlab

## Accounts & Access Requests

- [ ] Open AR issue for Sisense in `Editor` role.
  - See https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/13327 as an example
- [ ] Open AR issue to view SAFE-Intermediate dashboard
  - [ ] https://app.periscopedata.com/app/gitlab:safe-intermediate-dashboard/965418/MRARR-Diagnostics
  - [ ] https://app.periscopedata.com/app/gitlab:safe-intermediate-dashboard/965062/MRARR-Dashboard
  - [ ] https://app.periscopedata.com/app/gitlab:safe-intermediate-dashboard/965067/Wider-Community-Contribution-Dashboard
  - See https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/12209 as an example
- [ ] Follow the process to provision a Zendesk 'Light Agent' account
  - https://handbook.gitlab.com/handbook/support/internal-support/#requesting-a-zendesk-light-agent-account
- [ ] [Join](https://timezone.io/join/40487af2d39138f5) the Quality team's timezone.io group

## Merge Request Coach

- [ ] [become an MR coach](MR-COACH-ISSUE-URL)

## Review Contributor Growth Stand-up

- [ ] [Agenda](https://docs.google.com/document/d/1AOgqaslnq-WI1ICSZ1NzSnALf1Va4D5qAD191icAoSI/edit)

## Schedule 1-1s and coffee chats

Schedule coffee chats with:
- [ ] your manager, if not already on calendar, this should be a recurring weekly 1-1
- [ ] your onboarding buddy
- [ ] each of your team members
- [ ] Director of the Community Relations team.
- [ ] two MR coaches
- [ ] VP of Quality
- [ ] Director of Engineering Analytics
- [ ] Engineering Manager, Engineering Productivity
- [ ] Director, Quality Engineering
- [ ] Code Contributor Senior Program Manager

Pairing is a great way to get a general sense of the GitLab way and learn best practices.
Suggested focus is issue triaging, MR triaging and MR coaching.

- [ ] first issue triaging with a Contributor Success team member
- [ ] second issue triaging with a Contributor Success team member
- [ ] MR triaging with a Contributor Success team member
- [ ] MR triaging with an MR Coach not on the Contributor Success team
- [ ] Frontend Pairing session with someone NOT on the Contributor Success team
- [ ] Backend Pairing session with someone NOT on the Contributor Success team

## Improve our documentation

- [ ] Create an MR to update the [on-boarding issue template](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/blob/main/.gitlab/issue_templates/team-member-onboarding.md) with any additional items you completed for the next team member.

## First task: Make 10 Handbook improvements

- [ ] Handbook MR 1: (fill in MR URL here)
- [ ] Handbook MR 2: (fill in MR URL here)
- [ ] Handbook MR 3: (fill in MR URL here)
- [ ] Handbook MR 4: (fill in MR URL here)
- [ ] Handbook MR 5: (fill in MR URL here)
- [ ] Handbook MR 6: (fill in MR URL here)
- [ ] Handbook MR 7: (fill in MR URL here)
- [ ] Handbook MR 8: (fill in MR URL here)
- [ ] Handbook MR 9: (fill in MR URL here)
- [ ] Handbook MR 10: (fill in MR URL here)

## Finished!

Once you complete all of the steps above:

- [ ] please ping your manager in a comment
- [ ] close this issue


/label ~onboarding ~"Contributor Success"
