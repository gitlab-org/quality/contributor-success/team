<!-- Please use the following Issue title: Community Coworking day - Month Year - City, Country -->

@someone and @someone-else are heading to Somewhere for a Community Coworking day.

Note: This is an in-person event in City, Country

## Where

[WeWork TBC](https://www.wework.com/en-GB/search) Hot desk/day pass

([Wework on demand](https://www.wework.com/en-GB/solutions/wework-on-demand) is the easy way to get started)

## When

Second Tuesday of next month

## Messaging

Message in Discord [Event](#tbc):

```
Join us in London for a Community Coworking day. Come pair with us, or let us help you get 
started (or continue) with contributing!

Attendees will need to book their own Hot desk/Day pass for the day. More details can be 
found in the [issue](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/TBC)
```

## Sign up

Please sign up on the thread below if you want to join us!

/assign me
/label ~"community coworking day"
