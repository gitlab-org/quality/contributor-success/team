# Contributor Success team

Home of the [Contributor Success team](https://handbook.gitlab.com/handbook/marketing/developer-relations/contributor-success/)

See the [gitlab-org issues marked with contributor success](https://gitlab.com/groups/gitlab-org/-/boards/4296693) to understand what needs to be followed up in the context of gitlab-org

